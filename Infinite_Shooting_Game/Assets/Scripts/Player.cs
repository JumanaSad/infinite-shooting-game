﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private static Player instance;
    public int score=0;
    public Image healthBar; 
    public GameObject scoreText;

    Vector3 cameraInitialPosition;
    public float shakeMagnetude = 0.07f, shakeTime = 0.1f;
    public GameObject  mainCamera;
    public GameObject collidePanel;
    public GameObject gameOver;

    public void Start()
    {
        healthBar.fillAmount = 1;
        collidePanel.SetActive(false);
        gameOver.SetActive(false);
    }

    public static Player Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType(typeof(Player)) as Player;
            return instance;
        }
    }

    private void FixedUpdate()
    {
        // Healthbar's color is changing according to the health status
        if (healthBar.fillAmount < 0.005) {
            Time.timeScale = 0;
            gameOver.SetActive(true);
        }
        if (healthBar.fillAmount < 0.25f)
        {
            healthBar.color=Color.red;
        }
        else if (healthBar.fillAmount < 0.6f)
        {
            healthBar.color = Color.yellow;
        }
        else
        {
            healthBar.color = Color.green;
        }
        // Score:
        scoreText.GetComponent<Text>().text = "Score:" + score.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        healthBar.fillAmount-=0.05f;
        this.GetComponent<AudioSource>().Play();
        cameraInitialPosition = mainCamera.transform.position;
        InvokeRepeating("StartCameraShaking", 0f, 0.05f);
        Invoke("StopCameraShaking", shakeTime);

    }

    void StartCameraShaking()
    {
        float cameraShakingOffsetX = Random.value * shakeMagnetude * 2 - shakeMagnetude;
        float cameraShakingOffsetY = Random.value * shakeMagnetude * 2 - shakeMagnetude;
        Vector3 cameraIntermadiatePosition = mainCamera.transform.position;
        cameraIntermadiatePosition.x += cameraShakingOffsetX;
        cameraIntermadiatePosition.y += cameraShakingOffsetY;
        mainCamera.transform.position = cameraIntermadiatePosition;
        collidePanel.SetActive(true);
    }

    void StopCameraShaking()
    {
        CancelInvoke("StartCameraShaking");
        mainCamera.transform.position = cameraInitialPosition;
        collidePanel.SetActive(false);
    }


}
