﻿using UnityEngine;

public class SpawnedObject : MonoBehaviour
{
    public float speed;
    private void FixedUpdate()
    {
        transform.Translate((Player.Instance.transform.position - transform.position)
            * speed * Time.deltaTime);
    }

}
