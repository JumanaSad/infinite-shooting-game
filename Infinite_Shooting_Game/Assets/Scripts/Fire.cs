﻿using UnityEngine;


public class Fire : MonoBehaviour
{
    private int size = 50;
    RaycastHit hit;
    public float shotForce = 100f;

    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Player.Instance.mainCamera.GetComponent<ParticleSystem>().Play();

            if (Physics.Raycast(Player.Instance.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity))
            {
                if (hit.collider.tag == "Enemy")
                {
                    Player.Instance.score++;
                    Player.Instance.scoreText.GetComponent<Animator>().Play("Score");
                    hit.collider.gameObject.GetComponent<ParticleSystem>().Play();
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<AudioSource>().Play();
                    hit.collider.enabled = false;
                    Player.Instance.healthBar.fillAmount += 0.03f;
                }
            }
        }
#endif

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Player.Instance.mainCamera.GetComponent<ParticleSystem>().Play();

            if (Physics.Raycast(Player.Instance.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity))
            {
                if (hit.collider.tag == "Enemy")
                {
                    Player.Instance.score++;
                    Player.Instance.scoreText.GetComponent<Animator>().Play("Score");
                    hit.collider.gameObject.GetComponent<ParticleSystem>().Play();
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<AudioSource>().Play();
                    hit.collider.enabled = false;
                    Player.Instance.healthBar.fillAmount += 0.03f;
                }
            }
        }

    }




}
