# Infinite Game Shooter #
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)


* An FPS mobile shooting game created using google VR SDK and Unity.
* 1.0.0

# Gameplay #

Icosahedronal objects spawning in a range of 360 degree around the Player, The only way to survive 
is to direct the small red point towards those objects and shoot them before they reach you.

## Getting started

### Development Environment

* [Git](https://git-scm.com/downloads) (of course :D)
* [Unity 2019.4.19f1](https://unity3d.com/get-unity/download)
* [Visual Studio](https://visualstudio.microsoft.com/downloads)
* Locate [SDK](https://developer.android.com/studio/releases/platform-tools), [JDK](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html),[NDK](https://developer.android.com/ndk/downloads) inside the external tool window from the project preferences

### Installing

##### Select a folder and clone this repository using git

```
git clone https://JumanaSad@bitbucket.org/JumanaSad/infinite-shooting-game.git
```


### Run the app ###

* Using Editor: Click on run, "Space" key to shoot and "Alt" key with the mouse movement to rotate the screen.
* Using the app: download and enjoy

### Builds ###

* [v.1.0.0](https://bitbucket.org/JumanaSad/infinite-shooting-game/src/main/Infinite_Shooting_Game/Build/Infinite_Shooting_Game.apk)
